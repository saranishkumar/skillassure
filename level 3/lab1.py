# transpose of a matrix

M = 4
N = 3

# This function stores
# transpose of A[][] in B[][]

def transpose(A, B):

	for i in range(N):
		for j in range(M):
			B[i][j] = A[j][i]

# driver code
A = [ [1,2,3],[4,5,6],[7,8,9],[10,11,12]]


# To store result
B = [[0 for x in range(M)] for y in range(N)]

transpose(A, B)

print("Result matrix is")
for i in range(N):
	for j in range(M):
		print(B[i][j], " ", end='')
	print()

